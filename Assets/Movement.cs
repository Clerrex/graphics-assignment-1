﻿/* Project 1
 * Author: Michael Lumley 695059
 * Moves the camera using flight controls style movement
 */
using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed;
    public float torque;
    public Terrain terrain;

    private Vector3 movement;
    private Rigidbody rb;


    // Use this for initialization
    void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
        this.transform.position = new Vector3(terrain.terrainData.heightmapWidth / 2, terrain.terrainData.size.y + 50, terrain.terrainData.heightmapWidth / 2);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Mouse X");
        float moveVertical = Input.GetAxis("Mouse Y");
        Vector3 rotate;
        Vector3 movement = new Vector3(moveVertical, moveHorizontal, 0f);

        // Rotation
        if (Input.GetKey(KeyCode.Q)) {
            rotate = new Vector3(0f, 0f, 1f);
        }
        else if (Input.GetKey(KeyCode.E)) {
            rotate = new Vector3(0f, 0f, -1f);
        }
        else {
            rotate = Vector3.zero;
        }
        
        // Forwards and Back
        if (Input.GetKey(KeyCode.W)) {
            rb.AddForce(transform.forward * speed);
        }
        else if (Input.GetKey(KeyCode.S)) {
            rb.AddForce(-transform.forward * speed);
        }

        // Left and Right
        if (Input.GetKey(KeyCode.A)) {
            rb.position -= transform.right;
        }
        else if (Input.GetKey(KeyCode.D)) {
            rb.position += transform.right;
        }

        transform.Rotate(rotate);
        rb.AddRelativeTorque(movement * torque);
        restrictMovment();
    }

    void OnCollisionEnter() {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    // Limits movement to the bounds of the terrain
    void restrictMovment() {
        float top = terrain.terrainData.heightmapWidth;
        float bottom = 0f;
        float right = terrain.terrainData.heightmapWidth;
        float left = 0f;

        float offset = 1f;

        if(rb.position.z > top - offset) {
            rb.velocity = Vector3.zero;
            rb.position = new Vector3(rb.position.x, rb.position.y, rb.position.z - offset);
        }

        if (rb.position.z < bottom + offset) {
            rb.velocity = Vector3.zero;
            rb.position = new Vector3(rb.position.x, rb.position.y, rb.position.z + offset);
        }

        if (rb.position.x > right - offset) {
            rb.velocity = Vector3.zero;
            rb.position = new Vector3(rb.position.x - offset, rb.position.y, rb.position.z);
        }

        if (rb.position.x < left + offset) {
            rb.velocity = Vector3.zero;
            rb.position = new Vector3(rb.position.x + offset, rb.position.y, rb.position.z);
        }
    }
}
