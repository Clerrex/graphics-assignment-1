﻿/* Project 1
 * Author: Michael Lumley 695059
 * Rotates the sun around the Z axis
 */
 using UnityEngine;
using System.Collections;

public class RotateZ : MonoBehaviour {

    public float speed;
    public Terrain terrain;
	// Use this for initialization
	void Start () {
        // Set the centre of rotation for the sun 
        this.transform.position = new Vector3(terrain.terrainData.heightmapWidth / 2, 0f, terrain.terrainData.heightmapWidth / 2);
        // Set the starting positon relative to the centre
        this.transform.GetChild(0).transform.localPosition = new Vector3(-terrain.terrainData.heightmapWidth, 0f, 0f);

    }
	
	// Update is called once per frame
	void Update () {
        // Rotation the sun
        this.transform.localRotation *= Quaternion.AngleAxis(speed * Time.deltaTime, Vector3.back);
        // Tell the shader where the sun is
        terrain.materialTemplate.SetVector("_SunPosition", this.transform.GetChild(0).transform.position);
    }
}
