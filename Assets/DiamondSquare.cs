﻿/* Project 1
 * Author: Michael Lumley 695059
 * Generates a heightmap for use with a unity terrain object
 */

using UnityEngine;
using System.Collections;

public class DiamondSquare : MonoBehaviour {

    public Terrain terrain;
    public float roughness;
    public GameObject waterLevel;

    private int size;
    private float oldRoughness;
    private TerrainData td;

    // Use this for initialization
    void Start() {
        td = terrain.terrainData;
        oldRoughness = roughness;
        size = td.heightmapWidth;

        DSalgo();
    }

    // Update is called once per frame
    void Update() {
        // Generate a new map when the g key is presed
        if (Input.GetKeyDown(KeyCode.G)) {
            roughness = oldRoughness;
            DSalgo();
        }
    }

    // Creates a 2D array of floats between 0 and 1
    // using the diamond square algorithm to create
    // a heightmap which is loaded into a terrain object
    void DSalgo() {
        float[,] heightMap = new float[size, size];

        // Seed the corners with a random value
        float seed = Random.value;

        heightMap[0, 0] = seed;
        heightMap[size - 1, 0] = seed;
        heightMap[0, size - 1] = seed;
        heightMap[size - 1, size - 1] = seed;

        int x;
        int y;
        int sideLength;
        int halfSideLength;

        // Start with the whole map and progressively consider small areas (squares or diamonds)
        for (sideLength = size - 1; sideLength >= 2; sideLength /= 2) {

            halfSideLength = sideLength / 2;

            // Square
            // Take the average value of the corners of the square
            // and add a random amount then clamp that value between 0 and 1
            // and assign it to the centre of the square
            // X and Y are the top left of the square
            for (x = 0; x < size - 1; x += sideLength) {
                for (y = 0; y < size - 1; y += sideLength) {
                    float avg = (heightMap[x, y] +
                                 heightMap[x + sideLength, y] +
                                 heightMap[x, y + sideLength] +
                                 heightMap[x + sideLength, y + sideLength]) / 4;
                    heightMap[x + halfSideLength, y + halfSideLength] = Mathf.Clamp01(avg + ((Random.value * 2f * roughness) - roughness));
                }
            }

            // Diamond
            // Take the average value of all points of the diamond
            // and add a random amount then champ that value between 0 and 1
            // and assign it to the centre of the diamond
            // X and Y are the centre of the diamond
            for (x = 0; x < size - 1; x += halfSideLength) {
                for (y = (x + halfSideLength) % sideLength; y < size - 1; y += sideLength) {
                    // The mod and add size - 1 is so we can use values on the other side of the array
                    float avg = (heightMap[(x - halfSideLength + size - 1) % (size - 1), y] +
                                 heightMap[(x + halfSideLength) % (size - 1), y] +
                                 heightMap[x, (y + halfSideLength) % (size - 1)] +
                                 heightMap[x, (y - halfSideLength + size - 1) % (size - 1)]) / 4;
                    avg = Mathf.Clamp01(avg + ((Random.value * 2f * roughness) - roughness));
                    heightMap[x, y] = avg;

                    // Set the values on the opposite edge to the same to wrap edges
                    if (x == 0) {
                        heightMap[size - 1, y] = avg;
                    }
                    if (y == 0) {
                        heightMap[x, size - 1] = avg;
                    }
                }
            }
            // Make the map less rough over time
            roughness /= 2f;
        }

        Debug.Log("DS complete");

        // Find the highest and lowest values
        float max = 0f;
        float min = 1f;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (heightMap[i, j] > max) {
                    max = heightMap[i, j];
                }
                if (heightMap[i,j] < min) {
                    min = heightMap[i, j];
                }
            }
        }

        // Give height range and min height to the shader to calculate how to colour the terrain
        terrain.materialTemplate.SetFloat("_HeightRange", (max - min) * td.size.y);
        terrain.materialTemplate.SetFloat("_MinHeight", min * td.size.y);

        // Transform the water plane to the same height the shader starts using the underwater colour
        waterLevel.transform.position = new Vector3(td.heightmapWidth / 2, (0.2f * (max - min) * td.size.y) + min * td.size.y, td.heightmapWidth / 2);
        waterLevel.transform.localScale = new Vector3(td.heightmapWidth / 8, 1f, td.heightmapWidth / 8);
        td.SetHeights(0, 0, heightMap);
    }
}