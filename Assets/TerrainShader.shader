﻿// Original Cg/HLSL code stub copyright (c) 2010-2012 SharpDX - Alexandre Mutel
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// Adapted for COMP30019 by Jeremy Nicholson, 10 Sep 2012
// Adapted further by Chris Ewin, 23 Sep 2013
// Adapted further (again) by Alex Zable (port to Unity), 19 Aug 2016
// Adapted further (again again) by Michael Lumley, 9 Sep 2016

Shader "Unlit/TerrainShader"
{
	Properties
	{
		_LightColor("Light Color", Color) = (0, 0, 0)
		_SnowColor("Snow Color", Color) = (0, 0, 0)
		_GrassColor("Grass Color", Color) = (0, 0, 0)
		_SandColor("Sand Color", Color) = (0, 0, 0)
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float3 _LightColor;
			uniform float3 _SunPosition;
			uniform float3 _SnowColor;
			uniform float3 _GrassColor;
			uniform float3 _SandColor;
			uniform float _HeightRange;
			uniform float _MinHeight;

			struct vertIn
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float4 color : COLOR;
			};

			struct vertOut
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float4 worldVertex : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
			};

			// Vertex shader
			vertOut vert(vertIn v)
			{
				vertOut o;

				float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);
				float3 worldNormal = normalize(mul(transpose((float3x3)unity_WorldToObject), v.normal.xyz));
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;

				o.worldVertex = worldVertex;
				o.worldNormal = worldNormal;

				return o;
			}

			// Fragment shader
			fixed4 frag(vertOut v) : SV_Target {
				float3 interpNormal = normalize(v.worldNormal);

				// Ambient
				float Ka = 1;
				float3 amb = UNITY_LIGHTMODEL_AMBIENT.rgb * Ka;

				// Diffuse
				float fAtt = 1;
				float Kd = 1;
				float3 L = normalize(_SunPosition - v.worldVertex.xyz);
				float LdotN = dot(L, interpNormal);
				float3 dif = float3(0.0f,0.0f,0.0f);

				// Snow Colour
				if (v.worldVertex.y > (0.8f * _HeightRange) + _MinHeight) {
					dif = fAtt * _LightColor.rgb * Kd * _SnowColor.rgb * saturate(LdotN);
				}
				// Sand colour
				else if (v.worldVertex.y < (0.2f * _HeightRange) + _MinHeight + 5) {
					dif = fAtt * _LightColor.rgb * Kd * _SandColor.rgb * saturate(LdotN);
				}
				// Grass colour
				else{
					dif = fAtt * _LightColor.rgb * Kd * _GrassColor.rgb * saturate(LdotN);
				}

				// Specular
				float Ks = 1;
				float specN = 25;
				float3 V = normalize(_WorldSpaceCameraPos - v.worldVertex.xyz);
				float3 R = normalize((2.0 * LdotN * interpNormal) - L);
				float3 spe = fAtt * _LightColor.rgb * Ks * pow(saturate(dot(V, R)), specN);

				float4 returnColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
				returnColor.rgb = amb.rgb + dif.rgb + spe.rgb;

				return returnColor;
			}
			ENDCG
		}
	}
}
