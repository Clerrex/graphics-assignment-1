Michael Lumley: 695059

Controls:
Use WASD to move around and the mouse to point the camera.
G - Generates a new terrain

Note:
The shader has been adapted from the shader used in Lab 5 with the main 
difference being vertex colouring in the fragment shader.

The terrain must be a square with a side length of 2n + 1 and height 2m + 1
The terrain size can be set in the terrain component of the terrain object 
under the settings menu.


Description:
My implementation uses Unity's built in terrain object. This creates a surface 
in the world which I can then apply a heightmap to.

The heightmap is a 2D float array which has values ranging from 0 - 1. These 
values are generated using the Diamond-Square algorithm with a random seed so 
that every time the algorithm is run a new terrain is made.

The values are mapped onto the terrain where 1 is the terrain's max height and 
0 to 0.

The terrain has a material attached to it which in turn has the shader 
attached to it. The shader handles the lighting and the colouring of the 
terrain.

Colouring is based on vertical height where vertexes higher than 80% of the 
other vertexes are "Snow" coloured, lower than 20% are "underwater" coloured 
and the rest "grass" coloured.

Lighting is based on the Phong model with the direction coming from the 
position of the sun as it orbits the terrain achieved by setting the sun's 
position in the shader every update.

The camera uses a ridgebody to handle movement and collisions with the help of 
colliers on both the camera and the terrain.

The Water is a plane with a semi-transparent material attached.